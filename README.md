# START UP GUIDE

Note: Docker, Docker Compose, JProfiler 11 and JMeter should be already installed

Build WEKARest with 

```
mvn clean package
```

In the project folder, please execute the following to install the image if 
necessary and start the container:

```
docker-compose -f docker-compose.yml up
```
WEKA is automatically deployed and JProfiler is waiting for a connection.


To view the Rest API with Swagger-UI and verify WEKA is running, look at

```
http://172.17.0.1:8080/
```

# PROFILING

JProfiler is automatically installed within the WEKA container and waits for a 
connection when being deployed. 

To connect to the container, open JProfiler locally

Go to Start Center -> New Session

Give a name and click Attach -> Attach to remove JVM

Under settings, enter the IP adress of the docker container (172.17.0.1) and port 8849

Click OK, Evaluate and OK again, profiling should now work as expected

# LOAD TESTS WITH JMETER

Open JMeter and load the WEKARest_load_test.jmx file from the repository

Deploy WEKARest as explained above

Start load tests with rightclick->Start on the respective thread group

# SCALING WITH DOCKER

To create multiple instances of the WEKARest container, use the following command:

```
docker-compose -f docker-compose-scale.yml up --scale jguweka=5
```
The number at the end defines the amount of containers being deployed.

Each container has a limited memory of 1GB which is defined in the docker-compose
file. 

To view the Rest API look at 
```
http://172.17.0.1/
```
Note that for load tests with JMeter the port should now be removed, as the 
explicit port mapping is also removed in the docker-compose file. 

This way 
each request is directed to the proxy, which then redirects it to the respective
container. 
Verify the correct deployment of the containers with:
```
docker stats
```
which should show all containers running.






